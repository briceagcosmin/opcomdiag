/*********************************************************************************************************************
  @Author             : Bebe-Cosmin Briceag
  @Module Description : Under development !!!
  @Revision           : 2.0
  *******************************************************************************************************************

  Naming Convention - Below is presented the template used for naming convention
  
  @Public Function                 : <Module>_<TypeReturned>_<Description>
  @Private Function                : <Module>_<TypeReturned>_<Description>
  @Local Variables                 : <SizeOfType>_<Description>
  @Global Variables                : <Module>_<SizeOfType>_<Description>
  @Defines used as flags           : <Module>_<SizeOfType>_<DESCRIPTION>_<FLAG>          ((cast to)Decimal Number)
  @Defines used as masks           : <Module>_<SizeOfType>_<DESCRIPTION>_<MASK>          ((cast to)Hex Number)
  @Defines used as compiler switch : <Module>_<DESCRIPTION>                              (Decimal Number) 


********************************************************************************************************************/


/*----------------------------------------------- I N C L U D E S -------------------------------------------------*/
#include <linux/types.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/usb.h>
#include <linux/spinlock.h>
#include <linux/cdev.h>
#include <linux/slab.h>

/*----------------------------------------- E N D - O F - I N C L U D E S -----------------------------------------*/



/*------------------------------------------- M O D U L E - S C O P E ---------------------------------------------*/


/*------------------------------------ E N D - O F - M O D U L E - S C O P E --------------------------------------*/



/*-------------------------------------------- F I L E - S C O P E ------------------------------------------------*/
#define DT_uw_VENDOR_ID        ((u16)0x0403)
#define DT_uw_PRODUCT_ID       ((u16)0x4F50)


#define DT_str_LICENSE     "GPL"
#define DT_str_AUTHOR      "Bebe-Cosmin Briceag"
#define DT_str_VERSION     "1.0"
#define DT_str_DEVICE_NAME "USBDiag"

#define DT_u8_IN_ENDPOINT_ADDRESS  (u8)0x81
#define DT_u8_OUT_ENDPOINT_ADDRESS (u8)0x02
#define DT_u16_PACKET_SIZE         ((u16)0x0040)

MODULE_LICENSE(DT_str_LICENSE);
MODULE_AUTHOR (DT_str_AUTHOR);
MODULE_VERSION(DT_str_VERSION);

static int  __init DT_s32_Startup(void);
static void __exit DT_v_Cleanup(void);

static ssize_t DT_t_Read       (struct file *, char __user *, size_t, loff_t *);
static ssize_t DT_t_Write      (struct file *, const char __user *, size_t, loff_t *);
static long    DT_l_Ctrl       (struct file *, unsigned int, unsigned long);
static int     DT_s32_Open     (struct inode *, struct file *);
static int     DT_s32_Release  (struct inode *, struct file *);
static int     DT_s32_Probe    (struct usb_interface *ps_Interface,const struct usb_device_id *ps_Id);
static void    DT_v_Disconnect (struct usb_interface *ps_Interface);
				
				
typedef struct
{
  struct cdev   s_DevModel;
  spinlock_t    t_lock;
  u16           u16_PacketSize;
  u8            u8_inEndpointAddress;
  u8            u8_outEndpointAddress;
  
}Diag_t;


typedef struct usb_device_id   UsbDevId;
typedef        dev_t           DevNumber;
typedef struct class           UsbClass;
typedef struct file_operations DevOps;
typedef struct usb_driver      UsbDriver;


static UsbDevId  DT_s_GadgetInfo[] = {{USB_DEVICE(DT_uw_VENDOR_ID,DT_uw_PRODUCT_ID)},{}};
static UsbClass *DT_ps_DevClass;
static DevNumber DT_t_DevNumber; 
static Diag_t   *DT_ps_Diag;
static DevOps    DT_s_DevOps = {  .owner          = THIS_MODULE,
                                  .read           = DT_t_Read,
                                  .write          = DT_t_Write,
                                  .open           = DT_s32_Open,
                                  .release        = DT_s32_Release,
                                  .unlocked_ioctl = DT_l_Ctrl
                                };
static UsbDriver DT_s_UsbDriver = {
                                    .name       = DT_str_DEVICE_NAME,
                                    .probe      = DT_s32_Probe,
                                    .disconnect = DT_v_Disconnect,
                                    .id_table   = DT_s_GadgetInfo
                                  };

MODULE_DEVICE_TABLE(THIS_MODULE,DT_s_GadgetInfo);
/*--------------------------------------- E N D - O F - F I L E - S C O P E ---------------------------------------*/



/*--------------------------------- F U N C T I O N S - I M P L E M E N T A T I O N -------------------------------*/
int __init
DT_s32_Startup(void)
{
  
  s32 s32_RetValue = 0;

  s32_RetValue = alloc_chrdev_region(&DT_t_DevNumber,0,1,DT_str_DEVICE_NAME);

  if(s32_RetValue < 0 )
    {
      printk (KERN_ALERT "Device cannot be registered \n");
      goto __exit_;
    }else
    
    DT_ps_DevClass = class_create (THIS_MODULE,DT_str_DEVICE_NAME);
  
  DT_ps_Diag = kmalloc(sizeof(Diag_t),GFP_KERNEL);
  spin_lock_init(&DT_ps_Diag->t_lock);
  DT_ps_Diag->u16_PacketSize        = DT_u16_PACKET_SIZE ;
  DT_ps_Diag->u8_inEndpointAddress  = DT_u8_IN_ENDPOINT_ADDRESS;
  DT_ps_Diag->u8_outEndpointAddress = DT_u8_OUT_ENDPOINT_ADDRESS;
  
  if(!DT_ps_Diag)
    {
      printk (KERN_ALERT "Kmalloc failed\n");
    }

  cdev_init(&DT_ps_Diag->s_DevModel,&DT_s_DevOps);
  DT_ps_Diag->s_DevModel.owner = THIS_MODULE;

  s32_RetValue = cdev_add(&DT_ps_Diag->s_DevModel,DT_t_DevNumber,1);

  if(s32_RetValue < 0)
    {
      printk(KERN_ALERT "Device cannot be added\n");
      goto __exit_;
    }

  usb_register(&DT_s_UsbDriver);

  if (!device_create(DT_ps_DevClass,NULL,MKDEV(MAJOR(DT_t_DevNumber),0),NULL,DT_str_DEVICE_NAME))
    {
      printk(KERN_ALERT "Device cannot be created\n");
      goto __exit_;
    }
  
  
 __exit_:
  return s32_RetValue;
}

void __exit
DT_v_Cleanup(void)
{
  unregister_chrdev_region(DT_t_DevNumber,1);

  device_destroy(DT_ps_DevClass,MKDEV(MAJOR(DT_t_DevNumber),0));

  cdev_del(&DT_ps_Diag->s_DevModel);

  class_destroy(DT_ps_DevClass);

  usb_deregister(&DT_s_UsbDriver);

}


/**

*/
ssize_t DT_t_Read (struct file *ps_File, char __user *ps8_UserBuffer, size_t t_Size, loff_t *pt_Offset)
{
  ssize_t t_RetVal = (ssize_t)0;


  return t_RetVal;

}

/**

 */
ssize_t DT_t_Write (struct file *ps_File, const char __user *ps8_UserBuffer, size_t t_Size, loff_t *pt_Offset)
{
  ssize_t t_RetVal = (ssize_t)0;


  
  return t_RetVal;
}

/**

 */
long DT_l_Ctrl(struct file *ps_File, unsigned int u32_Cmd, unsigned long ul_Arg)
{
  long l_RetVal = (s64)0;


  return l_RetVal;
}

/**

 */
int DT_s32_Open (struct inode *ps_Inode, struct file *ps_File)
{
  int s32_RetVal = (s32)0;


  
  return s32_RetVal;
}

/**

 */
int DT_s32_Release (struct inode *ps_Inode, struct file *ps_File)
{
  int s32_RetVal = (s32)0;


  return s32_RetVal;
}

/**

 */
int DT_s32_Probe(struct usb_interface *ps_Interface,const struct usb_device_id *ps_Id)
{
  int s32_RetVal = (s32)0;

  printk(KERN_DEBUG "A new device was plugged with Vendor ID %X and Product ID %X\n",ps_Id->idVendor,ps_Id->idProduct);

  return s32_RetVal;
}


/**

 */
void DT_v_Disconnect(struct usb_interface *ps_Interface)
{
  

}

/*-------------------------- E N D - O F - F U N C T I O N S - I M P L E M E N T A T I O N ------------------------*/
module_init(DT_s32_Startup);
module_exit(DT_v_Cleanup);
